package com.example.discussion.controllers;

import com.example.discussion.models.Post;
import com.example.discussion.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
//

public class PostController {
    @Autowired
    PostService postService;

    //Creating a post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post post){
        postService.createPost(post);
        return new ResponseEntity<>("Post created sucessfully", HttpStatus.CREATED);
    }
    //Getting all posts
    @RequestMapping(value ="/posts",method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts()
    {
        return new ResponseEntity<>(postService.getPosts(),HttpStatus.OK);
    }
    //Deleting apost
    @RequestMapping(value = "/posts/{postid}", method= RequestMethod.DELETE)
    public ResponseEntity<Object>deletePost(@PathVariable Long postid){
        return postService.deletePost(postid);
    }
    //Update a post
     @RequestMapping(value= "/posts/{postid}", method= RequestMethod.PUT)
             public ResponseEntity<Object>updatePost(@PathVariable Long postid,@RequestBody Post post){
         return postService.updatePost(postid ,post);
    }
}

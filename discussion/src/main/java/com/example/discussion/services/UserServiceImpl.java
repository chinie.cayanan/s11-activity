package com.example.discussion.services;


import com.example.discussion.models.User;

import com.example.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    //Create user
    public void createUser(User user){
        userRepository.save(user);
    }
    //Get posts
    public Iterable <User> getUser(){
        return userRepository.findAll();
    }
    //Delete post

    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted sucessfully", HttpStatus.OK);
    }
    public ResponseEntity updateUser(Long id, User user){
        User userForUpdating = userRepository.findById(id).get();

       userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());
        userRepository.save(userForUpdating);

        return new ResponseEntity<>("user udated",HttpStatus.OK);
    }

}

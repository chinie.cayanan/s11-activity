package com.example.discussion.services;

import com.example.discussion.models.Post;
import com.example.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
// The @Service annotation will allow us to use the CRUD methods inherited from the CRUDRepository even thoug interfaces do not  contain implementaio/method bodies
public class PostServiceelmpl {
    @Autowired
    private PostRepository postRepository;
    //Create post
    public void createPost(Post post){
        postRepository.save(post);
    }
    //Get posts
    public Iterable <Post> getPost(){
        return postRepository.findAll();
    }
    //Delete post

    public ResponseEntity deletePost(Long id){
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted sucessfully", HttpStatus.OK);
    }
    public ResponseEntity updatePost(Long id, Post post){
        Post postForUpdating = postRepository.findById(id).get();

        postForUpdating.setTitle(post.getTitle());
        postForUpdating.setContent(post.getContent());
        postRepository.save(postForUpdating);

        return new ResponseEntity<>("post udated",HttpStatus.OK);
    }

}

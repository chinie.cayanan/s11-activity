package com.example.discussion.controllers;

import com.example.discussion.models.Post;
import com.example.discussion.models.User;
import com.example.discussion.services.PostService;
import com.example.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;

    //Creating a post
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user){
        return new ResponseEntity<>("User created sucessfully", HttpStatus.CREATED);
    }
    //Getting all users
    @RequestMapping(value ="/users",method = RequestMethod.GET)
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUser(),HttpStatus.OK);
    }
    //Deleting a post
    @RequestMapping(value = "/user/{userid}", method= RequestMethod.DELETE)
    public ResponseEntity<Object>deleteUser(@PathVariable Long userid){
        return userService.deleteUser(userid);
    }
    //Update a post
    @RequestMapping(value= "/users/{userid}", method= RequestMethod.PUT)
    public ResponseEntity<Object>updatePost(@PathVariable Long userid,@RequestBody User user){
        return userService.updateUser(userid ,user);
    }
}
